package RobotFilipe;
import robocode.*;

public class FilipeHIT extends Robot
{
	
	public void run() {
	
		while(true) {
			turnGunLeft(360);
			turnGunRight(360);
			ahead(120);
			turnRight(45);
			back(50);
			
		}
	}

	public void onScannedRobot(ScannedRobotEvent e) {
		if (e.getDistance() < 50 && getEnergy() > 50) {
			fire(3);
		}
		else {
			fire(1);
		}
		scan();
	}


	public void onHitByBullet(HitByBulletEvent e) {	
		turnRight(100);
		ahead(50);
		
		
		
	}
	public void onHitWall(HitWallEvent e) {
		ahead(100);
		turnGunRight(180);
		back(50);
		turnGunLeft(180);
		turnRight(90);
		
	}	
}
